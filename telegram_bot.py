import logging
from pprint import pprint

import requests
from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = 'TOKEN'

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    await message.reply("Hi!\nI'm EchoBot!\nPowered by aiogram.")


@dp.message_handler()
async def echo(message: types.Message):
    url = 'https://kitsu.io/api/edge/anime?filter[text]=' + message.text
    response = requests.get(url).json()
    for data in response['data']:
        anime = data['attributes']
        title = anime['canonicalTitle']
        rating = anime['popularityRank']
        poster = anime['posterImage']['large']

        await message.answer_photo(poster, title + '\n' + str(rating))


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
